library(corrplot)
library(caTools)
library(rpart)
library(rpart.plot)
library(randomForest)
library(pROC)
library(ggplot2)
library(dplyr)
library(lubridate)
library(tm)
library(ROSE)
setwd("C:/Users/מאיה וערן/Desktop/מבחן בית R")
#Q1
#1

cars.original <- read.csv('cars.csv')

cars <- cars.original

dim(cars)
str(cars)

#2 לחזוררררר

carsCallEnd.corpus <- Corpus(VectorSource(cars$CallEnd))
carsCallStart.corpus <- Corpus(VectorSource(cars$CallStart))
carsCallStart.corpus[[1]][[1]]

carsCallEnd.corpus <- tm_map(carsCallEnd.corpus, removePunctuation)
carsCallStart.corpus <- tm_map(carsCallStart.corpus, removePunctuation)


#3 יצירת דתה עם 10 תכונות
# CarInsurance המטרה
#job
#Age
#Education
#CarLoan
#NoOfContacts
#Marital
#HHInsurance
#Default

carsnew <- cars

carsnew$Id <- NULL
carsnew$Balance<-NULL
carsnew$Communication<-NULL
carsnew$LastContactDay<-NULL
carsnew$LastContactMonth<-NULL
carsnew$DaysPassed<-NULL
carsnew$PrevAttempts<-NULL
carsnew$Outcome<-NULL
carsnew$CallEnd<-NULL
carsnew$CallStart<-NULL


model <- lm(CarInsurance~ .,carsnew)

#Q2

str(carsnew)

carsnew$Default <- as.factor(carsnew$Default)
carsnew$HHInsurance <- as.factor(carsnew$HHInsurance)
carsnew$CarLoan <- as.factor(carsnew$CarLoan)
carsnew$CarInsurance <- as.factor(carsnew$CarInsurance)


table(carsnew$Education)
table(carsnew$Marital)
table(carsnew$CarInsurance)
table(carsnew$CarLoan)
table(carsnew$HHInsurance)
table(carsnew$Default)
table(carsnew$Job)

any(is.na(carsnew$Education))

make_Unknown<- function(x){
  if (is.na(x)) {
    return('Unknown')
  }
  return (x)
}

carsnew$Job <- sapply(carsnew$Job,make_Unknown)
carsnew <- carsnew[carsnew$Job!='Unknown',]

carsnew$Education <- sapply(carsnew$Education,make_Unknown)
carsnew <- carsnew[carsnew$Education!='Unknown',]



any(is.na(carsnew))
any(is.na(carsnew$CarInsurance))
any(is.na(carsnew$NoOfContacts))

# אין נתונים חסרים

ggplot(carsnew, aes(Education)) + geom_bar(aes(fill = CarInsurance))
ggplot(carsnew, aes(Marital)) + geom_bar(aes(fill = CarInsurance))

ggplot(carsnew, aes(Age)) + geom_bar(aes(fill = CarInsurance))

agetail <- function(x){
  if (x <= 75){
    return(x)}
  else{
    return(75)
  }
}
carsnew$Age <- sapply(carsnew$Age,agetail)

ggplot(carsnew, aes(NoOfContacts)) + geom_bar(aes(fill = CarInsurance))

NoOfContactstail <- function(x){
  if (x <= 20){
    return(x)}
  else{
    return(20)
  }
}

carsnew$NoOfContacts <- sapply(carsnew$NoOfContacts,NoOfContactstail)

#test train

filter <- sample.split(carsnew$CarInsurance, SplitRatio = 0.7)

#Training set 
carsnew.train <- subset(carsnew, filter==TRUE)

#test set 
carsnew.test <- subset(carsnew, filter==F)

dim(carsnew)
dim(carsnew.train)
dim(carsnew.test)


#Q3

#1

model.dt <- rpart(CarInsurance ~ ., carsnew.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

prediction <- predict(model.dt,carsnew.test)
actual <- carsnew.test$CarInsurance


#3
model.rf <- randomForest(CarInsurance ~ ., data = carsnew.train, importance = TRUE)

prediction.rf <- predict(model.rf,carsnew.test)
actual <- carsnew.test$CarInsurance

cf.rf <- table(actual,prediction.rf > 0.6)
precision <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
recall <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])

rocCurveDC <- roc(actual,prediction, direction = ">", levels = c('0', '1'))
rocCurveRF <- roc(actual,prediction.rf, direction = ">", levels = c('0', '1'))

#plot the chart 
plot(rocCurveDC, col = 'red',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveRF, col = 'blue',main = "ROC Chart")

auc(rocCurveDC)
auc(rocCurveRF)

#Q4

lastststus <- carsnew.test$CarInsurance
lastprob <-   (lastststus[1] ) / (lastststus[1]+ lastststus[2] )
lastprofit <- lastprob*200 - ((lastststus[1] + lastststus[2] )*100)


newcost <- (cf.rf[2,1]+cf.rf[1,2])*100
newprofit<- (cf.rf[2,2])*200 - newcost



#2

model.rf2 <- randomForest(CarInsurance ~ HHInsurance, data = carsnew.train, importance = TRUE)

prediction.rf2 <- predict(model.rf2,carsnew.test)
actual2 <- carsnew.test$CarInsurance

cf.rf2 <- table(actual2,prediction.rf2)
precision2 <- cf.rf2[1,1]/(cf.rf2[1,1] + cf.rf2[2,1])
recall2 <- cf.rf2[1,1]/(cf.rf2[1,1] + cf.rf2[1,2])







